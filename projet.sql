-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Client: 127.0.0.1
-- Généré le : Jeu 03 Mai 2012 à 13:13
-- Version du serveur: 5.5.20
-- Version de PHP: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `jaime`
--

CREATE TABLE IF NOT EXISTS `jaime` (
  `id_photo` int(11) NOT NULL,
  `id_pseudo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `jaime`
--

INSERT INTO `jaime` (`id_photo`, `id_pseudo`) VALUES
(83, 8),
(81, 8),
(89, 7),
(86, 7),
(80, 7);

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pseudo` int(11) NOT NULL,
  `url` varchar(500) NOT NULL,
  `commentaires_tof` varchar(500) NOT NULL,
  `mots_cle1` varchar(100) NOT NULL,
  `mots_cle2` varchar(100) NOT NULL,
  `mots_cle3` varchar(100) NOT NULL,
  `jaime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

--
-- Contenu de la table `photos`
--

INSERT INTO `photos` (`id`, `id_pseudo`, `url`, `commentaires_tof`, `mots_cle1`, `mots_cle2`, `mots_cle3`, `jaime`) VALUES
(75, 6, 'http://www.ladepeche.fr/content/photo/biz/2008/05/12/photo_1210611613919-1-0_zoom.jpg', 'pelerinage chartres', 'pele chartres', '', '', 0),
(76, 6, 'http://3.bp.blogspot.com/_eP-0_100pVM/SaQfh4994mI/AAAAAAAAHJg/fWdpAHOv2Ok/s400/20080511+P%2B%C2%AEl%2B%C2%AE+de+Chartres+035.jpg', '', 'pele chartres', '', '', 0),
(77, 6, 'http://www.nd-chretiente.com/2011/affiche2010_224.jpg', '', 'pele chartres', '', '', 0),
(78, 7, 'http://www.esseclive.com/partage/album/2006pelerinagechartres/2006pelerinagechartres1.jpg', '', 'chapelle', '', '', 0),
(79, 7, 'http://perso.wanadoo.fr/esprit.escalier/images/Chartres_pelerinage_1avril07.jpg', '', 'cathe', '', '', 0),
(80, 8, 'http://www.nd-chretiente.com/dotclear/public/Reportage_en_direct/Lundi-matin/.DSC_1034-b_m.jpg', '', 'peler', '', '', 1),
(81, 8, 'http://evry.catholique.fr/IMG/jpg/Affiche_EVRY-site.jpg', '', 'chartres', '', '', 1),
(82, 8, 'http://medias.lepost.fr/ill/2011/06/13/h-20-2521797-1307959865.jpg', 'tous a chartres', 'tous', '', '', 0),
(83, 8, 'http://www.saintlouisboulogne.com/wp-content/gallery/2008-pelerinage/10pelerinage2008.jpg', '', 'jska', '', '', 1),
(84, 8, 'http://www.dinosoria.com/tragedie/chartres_006.jpg', '', 'dsd', '', '', 0),
(85, 8, 'http://www.notredamedeparis.fr/IMG/jpg/affiche_chartres_2010_1_.jpg', '', 'etud', '', '', 0),
(86, 8, 'http://storage.canalblog.com/29/57/558766/53473796_p.jpg', '', 'scout', '', '', 1),
(87, 8, 'http://storage.canalblog.com/27/90/558766/53474413.jpg', '', 'asda', '', '', 0),
(88, 8, 'http://www.paroisse-stmartin.org/IMG/jpg/CHARTRES_-_portail_Nord_du_couronnement_de_la_Vierge.jpg', '', 'viege', '', '', 0),
(89, 8, 'http://www.cathedrale-chartres.org/images/photos/grandes/3417773028-c56928a9aa-b.jpg', '', 'pelerin', '', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `sexe` varchar(20) NOT NULL,
  `date_de_naissance` varchar(20) NOT NULL,
  `description` text,
  `password` varchar(20) NOT NULL,
  `nomvisible` varchar(10) DEFAULT NULL,
  `emailvisible` varchar(10) DEFAULT NULL,
  `nombre_connexion` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `pseudo`, `email`, `nom`, `prenom`, `sexe`, `date_de_naissance`, `description`, `password`, `nomvisible`, `emailvisible`, `nombre_connexion`, `score`) VALUES
(6, 'fleuka', 'leukamlakofranklin@yahoo.com', 'leukam', 'Franklin', 'Homme', '10  janvier 1994', '', 'nMy9tKh36E', 'oui', 'oui', 2, 5),
(7, 'floic', 'leukamlakofranklin@gmail.com', 'Loic', 'francis', 'Homme', '15  janvier 1998', '', '9PxJ4QgDuL', 'oui', 'oui', 2, 4),
(8, 'laet', 'laer@hotmail.com', 'laetitia', 'synthia', 'Femme', '15  aout 1993', '', 'wOrH5Dz6fA', 'oui', 'oui', 1, 14),
(9, 'Ivan', 'Inav@gmail.fr', 'Ivano', 'Ivan', 'Homme', '1  janvier 2012', 'blabla', 'vKfBcPb5f6', 'oui', 'oui', 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
